var chai = require('chai')
  , chaiHttp = require('chai-http');
var expect = require('chai').expect;
var http = require('http');
var app = require('../index.js');

chai.use(chaiHttp);

describe('helloworld.js', function () {
    it('should return hello world if / visited', function () {
         chai.request(app)
             .get('/')
             .end(function (err, res) {
              expect(err).to.be.null;
              expect(res.text).to.equal('Hello World!');
              expect(res).to.have.status(200);
         });
    });
});

describe('helloworld.js', function () {
    it('should return ok if /status visited', function () {
         chai.request(app)
             .get('/status')
             .end(function (err, res) {
              expect(err).to.be.null;
              expect(res.text).to.equal('OK');
              expect(res).to.have.status(200);
         });
    });
});

describe('helloworld.js', function () {
    it('should return page not found if invalid route visited', function () {
         chai.request(app)
             .get('/invalid')
             .end(function (err, res) {
              expect(err).to.be.null;
              expect(res).to.have.status(404);
         });
    });
});
